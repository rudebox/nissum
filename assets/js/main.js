$(document).ready(function() {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //toggle search
  $('.header__icon--search').click(function() {
    $('.header__form--wrapper').toggleClass('is-fixed');
    $('.header__icon--close').toggleClass('hidden');
    $('.header__input').show();
    $('.header__close').toggleClass('hidden');
    $('.header__btn--inner').toggleClass('hidden');
    $('body').toggleClass('is-fixed');
  });

  $('.header__icon--close').click(function() {
    $('.header__form--wrapper').removeClass('is-fixed'); 
    $('.header__input').hide();
    $(this).toggleClass('hidden');
    $('.header__btn--inner').toggleClass('hidden');
    $('body').toggleClass('is-fixed');
  });

  $('.header__icon--share').click(function() {
    $('.header__social').toggleClass('is-active');
  });

  $('.issuuembed').removeAttr('style');

  //responsive YT
  $('iframe').wrap("<div class='embed-container'></div>");

  //owl slider/carousel
  var owl = $(".slider__track");

  owl.each(function() {
    $(this).children().length > 1;

    $(this).owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        dots: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
    });
  });


  //fancybox
  $('[data-fancybox]').fancybox({ 
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    }  
  });


  //mixit up
   $(function(){ 
  
    var filterOnLoad = window.location.hash ? '.'+window.location.hash.substring(1) : 'all';
    
  });

  if($('body').is('.blog')){
    var mixer = mixitup('.mixit');
  }

  if($('body').is('.post-type-archive')){
   var mixer = mixitup('.mixit'); 
  }


 if ($(window).width() < 768) {
     $('.sidebar__title').click(function() {
        $('.sidebar').toggleClass('is-active'); 
     });
  }


  //instagram feed
  $.instagramFeed({ 
    'username': 'noerrenissumefterskole',
    'container': "#instagram-feed3",
    'display_profile': false,
    'display_biography': false,
    'display_gallery': true,
    'get_raw_json': false,
    'callback': null,
    'styling': true,
    'items': 6,
    'items_per_row': 6,
    'margin': 0
  });

  //Show hide select options bases on previous inputs - its a fucking retarded solution based on client wishes 
  //remember that form_id may change from local to live
  optionClassNames = ["", "football", "football", "football", "band", "band", "handball", "handball", "volleyball", "volleyball", "krea_design", "krea_design", "motor_metal", 
  "motor_metal", "gymnastics", "gymnastics", "fit_fun", "badminton", "badminton", "ballplay", "double_x", "guitar", "guitar", "food", "jump"];
  $.each($("#input_7_54 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]);
  });

  $.each($("#input_7_55 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  $.each($("#input_7_56 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  $.each($("#input_7_57 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  $.each($("#input_7_58 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  $.each($("#input_7_59 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  $.each($("#input_7_60 option"), function(index, value) {
      $(value).addClass(optionClassNames[index]); 
  });

  // add active class to selected option
  $(".gfield_select").change(function() {
    $(".gfield_select option:selected").addClass('is-active');
    $(".gfield_select option").not(':selected').removeClass('is-active');

    //football
    if($('.gfield_select > .football').hasClass('is-active') ) {
      $('.gfield_select > .football').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .football').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //band chosen
    if($('.gfield_select > .band').hasClass('is-active') ) {
      $('.gfield_select > .band').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .band').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //handball chosen
    if($('.gfield_select > .handball').hasClass('is-active') ) {
      $('.gfield_select > .handball').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .handball').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //krea_design chosen
    if($('.gfield_select > .krea_design').hasClass('is-active') ) {
      $('.gfield_select > .krea_design').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .krea_design').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //motor_metal chosen
    if($('.gfield_select > .motor_metal').hasClass('is-active') ) {
      $('.gfield_select > .motor_metal').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .motor_metal').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //gymnastics chosen
    if($('.gfield_select > .gymnastics').hasClass('is-active') ) {
      $('.gfield_select > .gymnastics').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .gymnastics').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //volleyball chosen
    if($('.gfield_select > .volleyball').hasClass('is-active') ) {
      $('.gfield_select > .volleyball').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .volleyball').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //fit_fun chosen
    if($('.gfield_select > .fit_fun').hasClass('is-active') ) {
      $('.gfield_select > .fit_fun').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .fit_fun').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //badminton chosen
    if($('.gfield_select > .badminton').hasClass('is-active') ) {
      $('.gfield_select > .badminton').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .badminton').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //ballplay chosen
    if($('.gfield_select > .ballplay').hasClass('is-active') ) {
      $('.gfield_select > .ballplay').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .ballplay').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //double_x chosen
    if($('.gfield_select > .double_x').hasClass('is-active') ) {
      $('.gfield_select > .double_x').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .double_x').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //guitar chosen
    if($('.gfield_select > .guitar').hasClass('is-active') ) {
      $('.gfield_select > .guitar').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .guitar').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //food chosen
    if($('.gfield_select > .food').hasClass('is-active') ) {
      $('.gfield_select > .food').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .food').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //jump chosen
    if($('.gfield_select > .jump').hasClass('is-active') ) {
      $('.gfield_select > .jump').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .jump').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

  });

  
  optionClassNames2 = ["", "change", "global", "klima", "move"];

  $.each($("#input_7_78 option"), function(index, value) {
      $(value).addClass(optionClassNames2[index]); 
  });

  $.each($("#input_7_79 option"), function(index, value) {
      $(value).addClass(optionClassNames2[index]); 
  });

  // add active class to selected option
  $(".gfield_select").change(function() {
    $(".gfield_select option:selected").addClass('is-active');
    $(".gfield_select option").not(':selected').removeClass('is-active');

    //change chosen
    if($('.gfield_select > .change').hasClass('is-active') ) {
      $('.gfield_select > .change').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .change').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //global chosen
    if($('.gfield_select > .global').hasClass('is-active') ) {
      $('.gfield_select > .global').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .global').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //klima chosen
    if($('.gfield_select > .klima').hasClass('is-active') ) {
      $('.gfield_select > .klima').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .klima').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //move chosen
    if($('.gfield_select > .move').hasClass('is-active') ) {
      $('.gfield_select > .move').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .move').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

  });


  optionClassNames3 = ["", "music", "choir", "climbing", "photo", "sos", "construction", "knit", "3d", "croque", "fire", "emusic", "codes", "theater", "spot"];

  $.each($("#input_7_98 option"), function(index, value) {
      $(value).addClass(optionClassNames3[index]); 
  });

  $.each($("#input_7_99 option"), function(index, value) {
      $(value).addClass(optionClassNames3[index]); 
  });

  $.each($("#input_7_100 option"), function(index, value) {
      $(value).addClass(optionClassNames3[index]); 
  });


   // add active class to selected option
  $(".gfield_select").change(function() {
    $(".gfield_select option:selected").addClass('is-active');
    $(".gfield_select option").not(':selected').removeClass('is-active');

    //music chosen
    if($('.gfield_select > .music').hasClass('is-active') ) {
      $('.gfield_select > .music').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .music').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //choir chosen
    if($('.gfield_select > .choir').hasClass('is-active') ) {
      $('.gfield_select > .choir').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .choir').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //climbing chosen
    if($('.gfield_select > .climbing').hasClass('is-active') ) {
      $('.gfield_select > .climbing').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .climbing').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //photo chosen
    if($('.gfield_select > .photo').hasClass('is-active') ) {
      $('.gfield_select > .photo').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .photo').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //construction chosen
    if($('.gfield_select > .construction').hasClass('is-active') ) {
      $('.gfield_select > .construction').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .construction').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //knit chosen
    if($('.gfield_select > .knit').hasClass('is-active') ) {
      $('.gfield_select > .knit').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .knit').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //knit chosen
    if($('.gfield_select > .3d').hasClass('is-active') ) {
      $('.gfield_select > .3d').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .3d').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //croque chosen
    if($('.gfield_select > .croque').hasClass('is-active') ) {
      $('.gfield_select > .croque').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .croque').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //fire chosen
    if($('.gfield_select > .fire').hasClass('is-active') ) {
      $('.gfield_select > .fire').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .fire').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //emusic chosen
    if($('.gfield_select > .emusic').hasClass('is-active') ) {
      $('.gfield_select > .emusic').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .emusic').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //codes chosen
    if($('.gfield_select > .codes').hasClass('is-active') ) {
      $('.gfield_select > .codes').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .codes').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //theater chosen
    if($('.gfield_select > .theater').hasClass('is-active') ) {
      $('.gfield_select > .theater').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .theater').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

    //spot chosen
    if($('.gfield_select > .spot').hasClass('is-active') ) {
      $('.gfield_select > .spot').addClass('hidden').attr("disabled", 'disabled');
      $('.is-active').removeAttr("disabled", 'disabled');
    }

    else {
     $('.gfield_select > .spot').removeClass('hidden').removeAttr("disabled", 'disabled');
    } 

  });

});
