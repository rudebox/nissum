<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>
  
  <section class="home padding--both">
    <div class="wrap hpad clearfix">

        <?php 
          // Custom WP query query
          $args_query = array(
            'meta_key'     => 'date',
            'meta_value'   => date( "Ymd" ), // change to how "event date" is stored
            'meta_compare' => '>',
            'order' => 'DESC',
          );

          $query = new WP_Query( $args_query );
        ?>

        <?php if ($query->have_posts()): ?>
          <?php while ($query->have_posts()): $query->the_post(); ?>

          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

          <a href="<?php the_permalink(); ?>" class="home__item" itemscope itemtype="http://schema.org/Event">

            <div class="home__thumb col-sm-5 col-md-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" >
              
            </div>
            
            <div class="col-sm-7 col-md-8 col-sm-offset-5 col-md-offset-4">
              <header>
                <h2 class="home__title" itemprop="name">
                    <?php the_title(); ?>
                </h2>
              </header>

              <div itemprop="about">
                <?php the_excerpt(); ?>

                <?php $date = get_field('date'); ?>
                
                <?php if ($date) : ?>
                  <p itemprop="startDate" class="home__meta"><time datetime="<?php echo esc_attr($date); ?>"><?php echo esc_html($date); ?></time></p> 
                <?php endif; ?>

                <span class="home__btn">Læs mere</span>
              </div>
            </div>

          </a>

          <?php wp_reset_postdata(); ?>

          <?php endwhile; else: ?>

            <p>Der er i øjeblikket ingen indlæg i denne kategori.</p>

        <?php endif; ?>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>