<?php

/*
 * Template Name: Layouts
 */

get_template_part('parts/header'); the_post(); ?>

<main class="clearfix">
	
	<?php get_template_part('parts/page', 'header');?>
	
	
	<?php 

		$show = get_field('show_sidebar'); 

		if ($show === true)   {

			echo '<div class="wrap">';
				echo '<div class="row">';

					get_template_part('parts/sidebar');

					echo '<div class="col-sm-8 col-sm-offset-1">';
						get_template_part('parts/content', 'layouts'); 
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}

		else {
			get_template_part('parts/content', 'layouts');
		}

	?>

</main>

<?php get_template_part('parts/footer'); ?>
