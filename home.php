<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>
  
  <section class="home padding--both">
    <div class="wrap hpad">
      <div class="row">

        <?php get_template_part('parts/sidebar-news'); ?>

        <div class="col-sm-8 col-sm-offset-1 mixit"> 

        <?php 
          // Custom WP query query
          $args_query = array(
            'order' => 'DESC',
            'category_name' => 'nyheder',
          );

          $query = new WP_Query( $args_query );
         ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>

            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>


            <?php 
            //Get category id to match up with sidebar
            $cats = get_the_category();
              $cat_string = "";
              foreach ($cats as $cat) {
                $cat_string .= " cat" . $cat->term_id ."";
              }
            ?>

            <a href="<?php the_permalink(); ?>" class="home__item mix <?php echo esc_attr($cat_string); ?>" itemscope itemtype="http://schema.org/BlogPosting">

              <div class="home__thumb col-sm-5 col-md-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" >
                
              </div>
              
              <div class="col-sm-7 col-md-8 col-sm-offset-5 col-md-offset-4">
                <header>
                  <h2 class="home__title" itemprop="headline">
                      <?php the_title(); ?>
                  </h2>
                </header>

                <div itemprop="articleBody">
                  <?php the_excerpt(); ?>

                  <p class="home__meta"><time datetime="<?php the_time('c'); ?>"><?php the_time('d.m.Y'); ?></time></p> 

                  <span class="home__btn">Læs mere</span>
                </div>
              </div>

            </a>

            <?php wp_reset_postdata(); ?>

            <?php endwhile; else: ?>

              <p>No posts here.</p>

          <?php endif; ?>

          </div>
        </div>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>