<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>
  
  <section class="single padding--both">
    <div class="wrap hpad">

      <article class="single__item" itemscope itemtype="http://schema.org/BlogPosting">

        <div itemprop="articleBody">
          <?php the_content(); ?>
          <a class="single__btn" onclick="window.history.go(-1); return false;">Tilbage</a>
        </div>

      </article>

    </div>
  </section>

  <?php get_template_part('parts/gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>