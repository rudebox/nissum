<?php get_template_part('parts/header'); ?>

<main class="clearfix">

<?php get_template_part('parts/page', 'header'); ?>

  <section class="downloads padding--both">
    <div class="wrap hpad">
      <div class="row">
      
      <?php get_template_part('parts/sidebar-download'); ?>
      
      <div class="col-sm-8 col-sm-offset-1 mixit">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>


          <?php 
            //Get category id to match up with sidebar
            $cats = get_the_category();
              $cat_string = "";
              foreach ($cats as $cat) {
                $cat_string .= " cat" . $cat->term_id ."";
              } 
          ?>

          <article class="downloads__item mix <?php echo esc_attr($cat_string); ?>" itemscope itemtype="http://schema.org/BlogPosting">

            <?php 
              $file_type = get_field('dl_filetype'); 
              $dl_filename = get_field('dl_filename'); 
              $dl_desc = get_field('dl_desc'); 
              $dl_mp3 = get_field('dl_mp3');
              $dl_pdf = get_field('dl_pdf');
              $dl_img = get_field('dl_img');
            ?>

            <header class="downloads__header">
              <h2 class="downloads__title" itemprop="headline">
                <?php the_title(); ?>
              </h2>
            </header>

            <div class="downloads__content" itemprop="articleBody">
              <?php echo esc_html($dl_desc); ?><br>

              <?php if ($file_type === 'pdf') : ?>
                <a class="downloads__btn" download href="<?php echo esc_url($dl_pdf); ?>">Download</a>
              <?php endif; ?>

              <?php if ($file_type === 'img') : ?>
                <img class="downloads__img" src="<?php echo esc_url($dl_img); ?>" alt="thumbnail">
                <a class="downloads__btn" download href="<?php echo esc_url($dl_img); ?>">Download</a>
              <?php endif; ?>

              <?php if ($file_type === 'mpthree') : ?>
                <?php echo do_shortcode('[audio src="' . $dl_mp3 . '"]'); ?>                
                <a class="downloads__btn" download href="<?php echo esc_url($dl_mp3); ?>">Download</a>
              <?php endif; ?>
            </div>

          </article>

          <?php endwhile; else: ?>

            <p>No posts here.</p>

        <?php endif; ?>
      </div>
    </div>

    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>