<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

<?php get_template_part('parts/page', 'header'); ?>

  <section class="search padding--both">
    <div class="wrap hpad clearfix">

      <h2 class="search__header">Søgeresultat for:</h2>
      <strong><p><?php echo esc_attr(get_search_query()); ?></p></strong>


      <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

          <a href="<?php the_permalink(); ?>" class="home__item" itemscope itemtype="http://schema.org/BlogPosting">
            
            <?php if ($thumb) : ?>
            <div class="home__thumb col-sm-5 col-md-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" >
              
            </div>
            <?php endif; ?>

            <?php if ($thumb) : ?>
            <div class="col-sm-7 col-md-8 col-sm-offset-5 col-md-offset-4">
            <?php else : ?>
            <div class="col-sm-12">
            <?php endif; ?>
              <header>
                <h2 class="home__title" itemprop="headline">
                    <?php the_title(); ?>
                </h2>
              </header>

              <div itemprop="articleBody">
                <?php the_excerpt(); ?>

                <span class="home__btn">Læs mere</span>
              </div>
            </div>

          </a>

        <?php endwhile; else: ?>

          <p>Vi kunne desværre ikke finde nogen resultater der matchede dit søgeresultat.</p>

      <?php endif; ?>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>