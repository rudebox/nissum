<aside id="sidebar" class="sidebar sidebar--download col-sm-3">
	<div class="sidebar--wrapper">

		<h3 class="sidebar__title border-title">	
			Download kategorier
			<i class="fas fa-angle-down"></i>
		</h3>

		<ul class="sidebar__children">
			<li class="sidebar__children--links" data-filter="all">Alle</li>
			<?php 
				//get download subcategories
				$args = array('child_of' => 9);
				$categories = get_categories( $args );
				foreach($categories as $category) { 
				  echo '<li data-filter=".cat' . $category->term_id . '" class="sidebar__children--links" href="#" title="' . sprintf( __( "Se alle indlæg i %s" ), $category->name ) . '" ' . '>' . $category->name.'</li> ';   
				    
				}
			 ?>
		</ul>

	</div>
</aside>