<aside id="sidebar" class="sidebar col-sm-3">
	<div class="sidebar--wrapper">

		<h3 class="sidebar__title border-title">	
			<?php 
				//get top level parent
				if ( 0 == $post->post_parent ) {
				    the_title();
				} else {
				    $parents = get_post_ancestors( $post->ID );
				    echo apply_filters( "the_title", get_the_title( end ( $parents ) ) );
				}
		 	?>
		 	<i class="fas fa-angle-down"></i>
		</h3>

		<?php 
			echo do_shortcode('[wpb_childpages]');
		?>

	</div>
</aside>