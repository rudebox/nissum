<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_field('gallery');
$index = get_row_index();

if ( $gallery ) : ?>

	<section class="gallery padding--bottom">
		<div class="wrap hpad">

			<div class="row gallery__list flex flex--wrap">

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<div class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url(<?php echo $image['url']; ?>)">
						<a href="<?= $image['sizes']['large']; ?>" class="js-zoom gallery__link" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
					<!-- 		<img class="gallery__image" data-src="<?= $image['sizes']['large']; ?>" src="<?= $image['sizes']['large']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>"> -->
						</a>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>