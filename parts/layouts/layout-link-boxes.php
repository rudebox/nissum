<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$title_center = get_sub_field('header_center');

if ($title_center === true) {
	$title_center = 'center';
} else {
	$title_center = '';
}

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="link-boxes__header <?php echo esc_attr($title_center); ?>"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$bg_color = get_sub_field('linkbox_bg');
				$link = get_sub_field('link');
			?>

			<div class="col-sm-3 link-boxes__item">

				<?php if ($link) : ?>
				<a href="<?php echo esc_url($link['url']); ?>" class="link-boxes__link center <?php echo esc_attr($bg_color); ?>--bg">
				<?php else : ?>
				<a class="link-boxes__link center <?php echo esc_attr($bg_color); ?>--bg">
				<?php endif; ?>
					
					<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				</a>

				<?php if ($text) : ?>
				<p class="link-boxes__text center"><?php echo $text; ?></p>
				<?php endif; ?>

			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>