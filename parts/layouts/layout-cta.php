<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//cta
$text = get_sub_field('text');
$link_text = get_sub_field('link_text');
$link = get_sub_field('link');
$header = get_sub_field('header');

?>

<section class="cta-layout <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>"> 

  <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bubbles_cta_small_left.svg'); ?>

  <div class="wrap hpad">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 center cta-layout__item">
        <h2 class="cta-layout__title"><?php echo esc_html($header); ?></h2>
        <?php echo $text; ?>
        <a class="btn btn--large" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
      </div>
    </div>
  </div>

  <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bubbles_cta_small_right.svg'); ?>
</section>