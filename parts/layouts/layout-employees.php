<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('employees_box') ) :
?>

<section class="employee padding--<?php echo esc_html($margin); ?> <?php echo esc_html($bg); ?>--bg">
	<div class="wrap hpad">
		<h2 class="employee__header"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('employees_box') ) : the_row();

					$img = get_sub_field('img');
					$name = get_sub_field('name');
					$position = get_sub_field('position');
					$mail = get_sub_field('mail');
					$phone = get_sub_field('phone');

 			 ?>

 			 <div class="col-sm-3 employee__item">
 			 	<img class="employee__img" src="<?php echo esc_url($img['sizes']['employee']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
 			 	<div class="employee__content">
	 			 	<h5 class="employee__name"><?php echo esc_html($name); ?></h5>
	 			 	<p class="employee__position"><?php echo esc_html($position); ?></p>
	 			 	<?php if ($mail) : ?>
	 			 	E-mail: <a class="employee__link" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>
	 			 	<?php endif; ?>
	 			 	<?php if ($phone) : ?>
	 			 	Tlf: <a class="employee__link" href="tel:<?php echo esc_html(get_formatted_phone(($phone))); ?>"> <?php echo esc_html($phone); ?></a>
	 			 	<?php endif; ?>
 			 	</div>
 			 </div>

 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
