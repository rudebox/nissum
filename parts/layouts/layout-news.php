<?php 
/**
* Description: Lionlab news field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');


// Custom WP query query
$args_query = array(
  'posts_per_page' => 4,
  'order' => 'DESC',
  'category_name' => 'nyheder',
);

$query = new WP_Query( $args_query );

//counter
$i=0;
?>


<?php  if ($query->have_posts() ) : ?>
<section class="news <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
  <div class="wrap hpad">
    <h2 class="news__header border-title"><?php echo esc_html($title); ?></h2>
    <div class="row flex flex--wrap news__row">

      <?php while ($query->have_posts() ) : $query->the_post(); 
          $i++; 

          if ($i === 1 || $i === 4 ) {
            $class = 'news__item--large';
          }

          else {
            $class = '';
          }
        ?>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );?>

        <a href="<?php the_permalink(); ?>" class="col-sm-6 news__item <?php echo esc_html($class); ?>" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);" itemscope itemtype="http://schema.org/BlogPosting">

          <header class="news__title">
            <h2 itemprop="headline"><?php the_title(); ?></h2>
            <p class="news__meta"><time datetime="<?php the_time('c'); ?>"><?php the_time('d.m.Y'); ?></time></p>
          </header>

          <div class="news__excerpt">
            <?php the_excerpt(); ?>
            <span class="btn btn--pulse news__btn"></span>
          </div>
        </a>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

    </div>

    <div class="center news__link">
      <a href="/nyheder" class="btn btn--large">Se alle nyheder</a>
    </div>
  </div>
</section>
<?php endif; ?>