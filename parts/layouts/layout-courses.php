<?php 
/**
* Description: Lionlab courses repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('courses_box') ) :
?>

<section class="courses <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="courses__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('courses_box') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
				$link_text = get_sub_field('link_text');
				$img = get_sub_field('icon');

				$image_id = $img['ID'];
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 courses__item center">
				
				<?php if ($img) : ?>
				<div class="courses__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
				</div>
				<?php endif; ?>
				
				<h3 class="courses__title"><?php echo esc_html($title); ?></h3>
				<?php if ($text) : ?>
					<?php echo $text; ?><br>
				<?php endif; ?>
				<span class="courses__btn"><?php echo esc_html($link_text); ?></span> 
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>