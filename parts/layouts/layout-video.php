<?php 
/**
* Description: Lionlab video field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//poster
$poster = get_sub_field('video_bg');

//video
$video_mp4 = get_sub_field('video_file_mp4');
$video_ogv = get_sub_field('video_file_ogv');
$video_webm = get_sub_field('video_file_webm');
?>

<?php if ($video_webm || $video_mp4 || $video_ogv || $poster) : ?>
<section class="video">
	<div class="video__wrap" style="background-image: url(<?php echo $poster['url']; ?>);">
				
		<video class="video__video" autoplay preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ" playsinline>
			<source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
			<source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
		</video>

		<div class="video__bubbles video__bubbles--blue-dark"></div>
		<div class="video__bubbles video__bubbles--blue-light"></div>
		<div class="video__bubbles video__bubbles--orange"></div>

	</div>
</section>
<?php endif; ?>