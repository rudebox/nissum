<?php get_template_part('parts/CTA-quote'); ?>

<section class="instagram">
	<div class="wrap--fluid clearfix">
		<div id="instagram-feed3" class="instagram_feed"></div>
	</div>
</section>

<footer class="footer blue-dark--bg" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>

	<?php 
  		$yt = get_field('yt', 'options');
  		$flickr = get_field('flickr', 'options');
  		$fb = get_field('fb', 'options');
  		$ig = get_field('ig', 'options');
  		$location = get_field('location', 'options');
 	?>

	<div class="footer__item--large wrap hpad flex flex--wrap flex--justify flex--valign">
		<div class="footer__copyright">
			Copyright © <?php echo date('Y'); ?>. Nørre Nissum Efterskole
		</div>
		<div class="footer__social">
			<a target="_blank" href="<?php echo esc_url($location); ?>"><i class="fas fa-map-marker-alt"></i></a>
			<a target="_blank" href="<?php echo esc_url($yt); ?>"><i class="fab fa-youtube"></i></a>
			<a target="_blank" href="<?php echo esc_url($flickr); ?>"><i class="fab fa-flickr"></i></a>
			<a target="_blank" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a>
			<a target="_blank" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a>
		</div>	
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
