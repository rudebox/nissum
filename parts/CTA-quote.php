<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//cta
$text = get_field('cta_text', 'options');
$link_text = get_field('cta_link_text', 'options');
$link = get_field('cta_link', 'options');

?>

<section class="cta">

  <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bubbles_cta_left.svg'); ?>

  <div class="wrap hpad">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 center cta__item">
        <?php echo $text; ?>
        <a class="btn btn--large" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
      </div>
    </div>
  </div>
  
  <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bubbles_cta_right.svg'); ?>
</section>

