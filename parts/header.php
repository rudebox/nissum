<!doctype html>

<html <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4MGBW6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php 
  $phone = get_field('phone', 'options');
  $mail = get_field('mail', 'options');
 ?>

<div class="toolbar blue-dark--bg">
  <div class="wrap hpad flex flex--wrap flex--end">

    <div class="toolbar__wrap flex flex--valign">
      <div class="toolbar__item">
        Tlf: <a href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
      </div>
      <div class="toolbar__item">
        E-mail: <a href="amilto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
      </div>
    </div>
    <div class="tooolbar__wrap hidden-sm">
      <?php scratch_toolbar_nav(); ?>
    </div>

  </div>
</div>

<header class="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="header__container wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_round.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <nav class="nav flex flex--valign" role="navigation">

      <div class="nav--mobile">
        <div class="wrap hpad">
        <?php scratch_main_nav(); ?>
        </div>
      </div>
    
      <?php scratch_quicklinks_nav(); ?>
      
      <div class="header__form--wrapper">
        <div class="wrap hpad">
          <form class="header__form" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">       
  
                <i class="header__icon header__icon--close fas fa-times hidden"></i>

                <label class="header__label">
                  <button type="button" class="header__btn header__btn--inner hidden"><i class="fas fa-search"></i></button>
                  <input class="header__input" type="text" value="<?php the_search_query(); ?>" placeholder="Indtast søgeord..." name="s" id="search"></input>
                </label>
              
          </form>
        </div>
      </div>

      <?php 
          //social links
          $yt = get_field('yt', 'options');
          $flickr = get_field('flickr', 'options');
          $fb = get_field('fb', 'options');
          $ig = get_field('ig', 'options');
          $location = get_field('location', 'options');
      ?>

      <div class="header__icon header__icon--share"><i class="fas fa-share-alt"></i>
        <div class="header__social--wrap">
          <div class="header__social">

            <a target="_blank" href="<?php echo esc_url($yt); ?>"><i class="fab fa-youtube"></i></a>
            <a target="_blank" href="<?php echo esc_url($flickr); ?>"><i class="fab fa-flickr"></i></a>
            <a target="_blank" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a>
            <a target="_blank" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a>
          </div>
        </div>
      </div>

      <a class="header__icon header__icon--search"><i class="fas fa-search"></i></a>        

      <div class="nav-toggle"> 
        <span class="nav-toggle__icon"></span>
      </div>
    </nav>

  </div>
</header>
