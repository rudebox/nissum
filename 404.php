<?php get_template_part('parts/header'); ?>

<main>
  <?php get_template_part('parts/page', 'header');?>

  <section class="wrap hpad clearfix padding--both">

    <h2 class="wysiwygs__title">Beklager, men vi kunne ikke finde siden du søgte efter.</h2>

    <a class="btn btn--large" href="/">Tilbage til forsiden</a>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>